const querystring = require("querystring")
const { set, get } = require("./src/db/redis")
const { access } = require("./src/utils/log")
const handleBlogRouter = require("./src/router/blog")
const handleUserRouter = require("./src/router/user")

// 获取cookie过期时间
const getCookieExpires = () => {
  const d = new Date()
  d.setTime(d.getTime() + 24 * 60 * 60 * 100)
  return d.toGMTString()
}

// const SESSION_DATA = {}
// 用来处理postData
const getPostData = (req) => {
  const promise = new Promise((resolve, reject) => {
    if (req.method !== "POST") {
      resolve({})
      return
    }

    if (req.headers["content-type"] !== "application/json") {
      resolve({})
      return
    }

    let postData = ""

    req.on("data", (chunk) => {
      postData += chunk.toString()
    })

    req.on("end", () => {
      if (!postData) {
        resolve({})
        return
      }

      resolve(JSON.parse(postData))
    })
  })

  return promise
}

const serverHandle = (req, res) => {
  // 记录 access log
  access(`${req.method} -- ${req.url} -- ${req.headers['user-agent']} -- ${Date.now()}`)
  res.setHeader("Content-type", "application/json")

  // 获取path
  const url = req.url
  const path = url.split("?")[0]
  req.path = path

  // 解析query
  req.query = querystring.parse(url.split("?")[1])

  // 解析cookie
  req.cookie = {}
  const cookieStr = req.headers.cookie || ""
  cookieStr.split(";").forEach((item) => {
    if (!item) {
      return
    }
    const arr = item.split("=")
    const key = arr[0].trim()
    const value = arr[1].trim()
    req.cookie[key] = value
  })

  // 解析session (使用redis)
  let needSetCookie = false
  let userId = req.cookie.userid
  if (!userId) {
    needSetCookie = true
    userId = `${Date.now()}_${Math.random()}`

    // 初始化session
    set(userId, {})
  }

  req.sessionId = userId
  // 获取session
  get(userId)
    .then((sessionData) => {
      if (sessionData == null) {
        // 初始化 redis 中的 session 值
        set(req.sessionId, {})
        req.session = {}
      } else {
        req.session = sessionData
      }

      // 处理postData
      return getPostData(req)
    })
    .then((postData) => {
      req.body = postData

      // 处理 blog 路由
      const blogResult = handleBlogRouter(req, res)

      if (blogResult) {
        if (needSetCookie) {
          res.setHeader(
            "Set-Cookie",
            `userid=${userId}; path=/; httpOnly; expires=${getCookieExpires()}`
          )
        }
        blogResult.then((blogData) => {
          res.end(JSON.stringify(blogData))
        })
        return
      }

      // 处理 user 路由
      const userResult = handleUserRouter(req, res)

      if (userResult) {
        if (needSetCookie) {
          res.setHeader(
            "Set-Cookie",
            `userid=${userId}; path=/; httpOnly; expires=${getCookieExpires()}`
          )
        }
        userResult.then((userData) => {
          res.end(JSON.stringify(userData))
        })
        return
      }

      // 未命中路由, 返回404
      res.writeHead(404, { "Content-type": "text/plain" })
      res.write("404 Not Found!")
      res.end()
    })

  //   if (userId) {
  //     if (!SESSION_DATA[userId]) {
  //       SESSION_DATA[userId] = {}
  //     }
  //   } else {
  //     needSetCookie = true
  //     userId = `${Date.now()}_${Math.random()}`
  //     SESSION_DATA[userId] = {}
  //   }
  //   req.session = SESSION_DATA[userId]
}

module.exports = serverHandle
