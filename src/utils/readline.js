const fs = require("fs")
const path = require("path")
const readline = require("readline")

const fullName = path.join(__dirname, "../", "../", "logs", "access.log")

// 创建read stream
const readStream = fs.createReadStream(fullName)

// 创建 readline 对象
const rl = readline.createInterface({ input: readStream })

let chromeNum = 0
let sum = 0

// 逐行读取
rl.on("line", (lineData) => {
  if (!lineData) {
    return
  }

  // 记录总行数
  sum++
  const arr = lineData.split(" -- ")
  if (arr[2] && arr[2].includes("Chrome")) {
    // 累加 chrome 数量
    chromeNum++
  }
})

// 监听读取完成
rl.on("close", () => {
  console.log(chromeNum / sum)
})
